from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
import time

# Create your tests here.

class AnehUnitTest(TestCase):
    def test_my_profile_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_my_profile_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'test.html')

    def test_subscribe_url_is_exist(self):
        response = Client().get('/subscribe')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_using_subscribe_function(self):
        found = resolve('/subscribe')
        self.assertEqual(found.func, subscribe)

    def test_using_subscribe_template(self):
        response = Client().get('/subscribe')
        self.assertTemplateUsed(response, 'subscribe.html')

    # def test_check_exist_url_is_exist(self):
    #     response = Client().get('/checkExist')
    #     self.assertEqual(response.status_code, 200)

    def test_checkExist_using_checkExist_function(self):
        found = resolve('/checkExist')
        self.assertEqual(found.func, checkExist)

    # def test_regist_url_is_exist(self):
    #     response = Client().get('/regist')
    #     self.assertEqual(response.status_code, 200)
        
    def test_regist_using_registSubscribe_function(self):
        found = resolve('/regist')
        self.assertEqual(found.func, registSubscribe)
