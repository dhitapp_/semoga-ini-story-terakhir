from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import mySubscribeModel
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_protect

# Create your views here.
response = {}

def index(request):
    return render(request, 'test.html', {})

@csrf_protect
def subscribe(request):
    return render(request, 'subscribe.html', {})

def checkExist(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = mySubscribeModel.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})
def registSubscribe(request):
    if (request.method == 'POST'):
        response['name'] = request.POST['name']
        response['email'] = request.POST['email']
        response['password'] = request.POST['password']
        mySubscribeModel.objects.create(name=response['name'], email=response['email'], password=response['password'])
        return JsonResponse({'is_success': True})