var email_exists = true;
var error_name = true;
var error_email = true;
var error_pass = true;
var already_name = false;
var already_email = false;
var already_pw = false;

$(document).ready(function () {
    $("#nameHelp").hide();
    $("#emailHelp").hide();
    $("#passHelp").hide();

    $("#submit").attr("disabled", "disabled");
    
    $("#inputName").focusout(function() {
        validateName();
    });

    $("#inputEmail").focusout(function() {
        validateEmail();
    });

    $("#inputPass").focusout(function() {
        validatePass()
    });

    var csrftoken = Cookies.get('csrftoken');
    
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('#form').on('submit', function (event) {
        event.preventDefault();
        sendForm();
    });

});

function validateName() {
    var nama = document.getElementById("inputName").value;
    already_name = true;
    if (nama.length < 2 ) {
        $("#nameHelp").show();
        error_name =  true;
    } else {
        $("#nameHelp").hide();
        error_name = false;
        clickable();
    }
};

function validateEmail() {
    var email = document.getElementById("inputEmail").value;
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    already_email = true;
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 > email.length) {
        $("#emailHelp").show();
        error_email = true;
    } else {
        $("#emailHelp").hide();
        checkExist();
        error_email = false;
        clickable();
    }
};

function validatePass() {
    var pass = document.getElementById("inputPass").value;
    already_pw = true;
    if (pass.length < 8) {
        $("#passHelp").show();
        error_pass = true;
    } else {
        $("#passHelp").hide();
        error_pass = false;
        clickable();
    }
};

function clickable() {
    if (!email_exists && !error_name && !error_email && !error_pass) {
        document.getElementById("submit").disabled = false;
    }
}

function checkExist() {
    $.ajax({
        method: 'POST',
        url: "/checkExist",
        data: {
            email: $('#inputEmail').val(),
        },
        success: function (email) {
            if (email.is_exists) {
                email_exists = true;
                $("#emailHelp").text("Email sudah pernah terdaftar!")
                $("#emailHelp").show()
            } else {
                email_exists = false;
            }
        },
        error: function () {
            alert("Error, tidak bisa memvalidasi email!")
        }
    })

};

function sendForm() {
    $.ajax({
        method: 'POST',
        url: "/regist",
        data: {
            name: $('#inputName').val(),
            email: $('#inputEmail').val(),
            password: $('#inputPass').val(),
        },
        success: function (response) {
            if (response.is_success) {
                location.href = "subscribe"
                alert("Terima kasih sudah subscribe halaman ini!")
            } else {
                alert("Data tidak berhasil tersimpan. Harap daftar ulang!")
            }
        },
        error: function () {
            alert("Data tidak berhasil tersimpan. Harap daftar ulang!");
        }
    })
}



