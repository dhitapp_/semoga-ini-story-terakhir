from django.urls import path
from .views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#url for app

urlpatterns = [
    path('', index, name = "index"),
    path('subscribe', subscribe, name = "subscribe"),
    path('checkExist', checkExist, name = "checkExist"),
    path('regist', registSubscribe, name="registSubscribe"),
]