from django import forms

class myDiaryForm(forms.Form):

    attrs = {
        'class': 'form-control'
    }

    tanggal = forms.DateTimeField(label='Tanggal', widget=forms.DateTimeInput(attrs={'type':'datetime-local','class': 'form-control'}), input_formats=('%d/%m/%Y %H:%M'),required = True)
    catatan = forms.CharField(label='Catatan', max_length=200, widget=forms.TextInput(attrs=attrs), required = True)
    