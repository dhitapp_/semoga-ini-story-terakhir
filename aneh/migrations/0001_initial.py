# Generated by Django 2.1.1 on 2018-11-19 04:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='myDiaryModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('catatan', models.CharField(max_length=200)),
                ('tanggal', models.DateTimeField()),
            ],
        ),
    ]
